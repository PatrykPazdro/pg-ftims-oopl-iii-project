package com.tank.map;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by kongo on 09.01.16.
 */
public interface TilesData {
    Tile border = new Tile(Color.DARK_GRAY, false, false);
    Tile ground = new Tile(Color.BROWN, true, false);
    Tile metal = new Tile(Color.SLATE, false, false);
}
