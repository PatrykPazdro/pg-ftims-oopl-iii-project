package com.tank.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by kongo on 10.01.16.
 */
public class Tank extends MoveableEntity {
    private EntityFactor entityFactor;
    protected boolean canShoot = true;
    protected float timeToShoot = 0;
    private float shootCooldown = 0.5f;
    private float attackDamage = 1;
    private float shootPower = 5;

    public Tank(String tag, Vector2 position, Color color, float width, float height, float maxVelocity) {
        super(tag, position, color, width, height, maxVelocity);
        entityFactor = EntityFactor.getInstance();
    }

    @Override
    public void update(float deltaTime){
        super.update(deltaTime);

        timeToShoot -= deltaTime;
        if(timeToShoot <0) {
            canShoot = true;
            timeToShoot = 0;
        }
    }

    @Override
    public void render(ShapeRenderer shapeRenderer){
        super.render(shapeRenderer);
        shapeRenderer.setColor(Color.GRAY);
        float w1 = 0.4f, w2 = 0.2f;
        float h1 = 0.5f, h2 = 0.7f;
        if(dir == Dir.north)
            shapeRenderer.rect(position.x + w1, position.y + h1, w2, h2);
        else if(dir == Dir.south)
            shapeRenderer.rect(position.x + w1, position.y - (h2 - h1), w2, h2);
        else if(dir == Dir.west)
            shapeRenderer.rect(position.x - (h2 - h1), position.y + w1, h2, w2);
        else
            shapeRenderer.rect(position.x + h1, position.y + w1, h2, w2);
    }

    protected void shoot(){
        if(canShoot) {
            int dir[] = dirToXY();
            entityFactor.addProjectile("bullet", position.x + 0.25f,position.y + 0.25f,dir[0] * shootPower,
                    dir[1] * shootPower, this, attackDamage);
            timeToShoot = shootCooldown;
            canShoot = false;
        }
    }
}
