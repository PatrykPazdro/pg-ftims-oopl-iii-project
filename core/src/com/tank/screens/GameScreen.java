package com.tank.screens;

import com.tank.Main;
import com.tank.entities.EntityFactor;
import com.tank.entities.Player;
import com.tank.map.Map;

/**
 * Created by kongo on 09.01.16.
 */
public class GameScreen extends MyScreen {
    private Map map;
    private EntityFactor entityFactor;
    private Player player;

    public GameScreen(Main main) {
        super(main);
    }

    @Override
    public void show(){
        super.show();

        map = Map.getInstance();
        map.init(main.width, main.height);

        entityFactor = EntityFactor.getInstance();
        entityFactor.init();

        player = (Player) entityFactor.addPlayer("player", 1,1);
        entityFactor.addEnemy("enemy", map.getWidth()/2-2, map.getHeight()/2-2, player);

        // TagException test
//        try {
//            entityFactor.getEntityByTag("et");
//        } catch (TagException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void render(float delta){
        if(!entityFactor.isGameover()) {
            entityFactor.update(delta);

            super.render(delta);
            map.render(shapeRenderer);
            entityFactor.render(shapeRenderer);
        }
        else{
            main.setScreen(new GameOverScreen(main, entityFactor.isWin()));
        }
    }
}
