# TankArena #
## Author: Patryk Pazdro ##
### Description ###
It is game where you are a tank and fight against another tank(similar to nes Battle City but without base).

### Controls ###
Arrows - moving.
Z - shooting.

### Importing and Running Project in Intellij IDEA ###
Go to Import Project, navigate to project folder and select the build.gradle file. Hit OK. In the next dialog, leave all settings as they are and hit OK again. It will now import project. This can take a while on the first time, as it downloads the Gradle wrapper and some dependencies.

Desktop: Run -> Edit Configurations..., click the plus (+) button and select Application. Set the Name to Desktop. Set the field Use classpath of module to desktop, then click on the button of the Main class field and select the DesktopLauncher class. Set the Working directory to /core/assets/ folder. Click Apply and then OK. You have now created a run configuration for desktop project. You can now select the configuration and run it.