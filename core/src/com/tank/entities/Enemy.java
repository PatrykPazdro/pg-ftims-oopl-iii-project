package com.tank.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by kongo on 10.01.16.
 */
public class Enemy extends Tank {
    private Player player;
    private float xNext;
    private float yNext;
    private Array<AIPoint> adj_spaces;
    private Array<AIPoint> options;

    public Enemy(String tag, Vector2 position, Player player) {
        super(tag, position, Color.RED, 1, 1, 3);
        this.player = player;

        xNext = (int) position.x;
        yNext = (int) position.y;
        adj_spaces = new Array<AIPoint>();
        options = new Array<AIPoint>();
    }

    public Enemy(String tag, Vector2 position, Color color, float width, float height, float maxVelocity, Player player) {
        super(tag, position, color, width, height, maxVelocity);
        this.player = player;

        xNext = (int) position.x;
        yNext = (int) position.y;
        adj_spaces = new Array<AIPoint>();
        options = new Array<AIPoint>();
    }

    @Override
    public void update(float deltaTime) {
        AI(deltaTime);
        super.update(deltaTime);
    }

    @Override
    protected void mapCollision(float deltaTime){}

    private void AI(float deltaTime) {
        float vx = Math.abs(velocity.x * deltaTime);
        if(Math.abs(xNext - position.x) < vx )
            position.x = xNext;
        float vy = Math.abs(velocity.y * deltaTime);
        if(Math.abs(yNext - position.y) < vy )
            position.y = yNext;

        AIPointPool.freeAll(adj_spaces);
        adj_spaces.clear();
        adj_spaces.add(AIPointPool.obtain().set( (position.x - 0.5f), position.y, Dir.west));
        adj_spaces.add(AIPointPool.obtain().set( position.x,  (position.y - 0.5f), Dir.south));
        adj_spaces.add(AIPointPool.obtain().set( position.x + width + 0.5f,  position.y, Dir.east));
        adj_spaces.add(AIPointPool.obtain().set( position.x,  position.y + height + 0.5f, Dir.north));

        options.clear();
        for (int j = 0; j < adj_spaces.size; j++) {
            AIPoint space = adj_spaces.get(j);
            if (!map.collide((int)(space.x * 2), (int)(space.y * 2))) {
                if((space.dir == Dir.west || space.dir == Dir.east) && !map.collide((int)(space.x * 2), (int)(space.y * 2) + 1))
                    options.add(space);
                else if((space.dir == Dir.north || space.dir == Dir.south) && !map.collide((int)(space.x * 2) + 1, (int)(space.y * 2)))
                    options.add(space);
            }
        }
        if(position.x == xNext && position.y == yNext) {
            chase(player.position);
        }

        shoot();
    }

    @Override
    protected void shoot(){
        if(canShoot) {
            float x = position.x - player.position.x;
            float y = position.y - player.position.y;
            if(Math.abs(x) > Math.abs(y)){
                if(x>0)
                    dir = Dir.west;
                else
                    dir = Dir.east;
            }
            else{
                if(y>0)
                    dir = Dir.south;
                else
                    dir = Dir.north;
            }
            super.shoot();
        }
    }

    private void chase(Vector2 targetPosition) {
        Vector2 target = new Vector2(targetPosition.x, targetPosition.y);
        float minDistance = map.getHeight() * map.getHeight() * 4;
        AIPoint nextPosition = null;
        for (int i = 0; i < options.size; i++) {
            AIPoint opt = options.get(i);
            Vector2 o = new Vector2(opt.x, opt.y);
            float dist = o.dst(target);
            if (dist < minDistance) {
                nextPosition = opt;
                minDistance = dist;
            }
        }

        if(nextPosition != null){
            xNext = nextPosition.x;
            yNext = nextPosition.y;

            int[] v = dirToXY(nextPosition.dir);
            velocity.x = v[0] * maxVelocity;
            velocity.y = v[1] * maxVelocity;
        }
    }

    private Pool<AIPoint> AIPointPool = new Pool<AIPoint>() {
        @Override
        protected AIPoint newObject() {
            return new AIPoint();
        }
    };

    private class AIPoint{
        float x;
        float y;
        Dir dir;

        public AIPoint(){};

        public AIPoint set(float x, float y, Dir dir) {
            this.x = x;
            this.y = y;
            this.dir = dir;

            return this;
        }
    }
}
