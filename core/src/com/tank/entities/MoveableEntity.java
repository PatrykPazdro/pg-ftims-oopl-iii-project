package com.tank.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.tank.Utils;
import com.tank.map.Map;

/**
 * Created by kongo on 09.01.16.
 */
public class MoveableEntity extends Entity {
    public enum Dir {
        north, west, east, south
    }

    protected Vector2 velocity;
    protected float maxVelocity;
    protected Dir dir = Dir.south;
    protected static Array<Rectangle> tiles = new Array<Rectangle>();
    protected Map map;

    public MoveableEntity(String tag, Vector2 position, Color color, float width, float height, float maxVelocity) {
        super(tag, position, color, width, height);
        this.map = Map.getInstance();
        velocity = new Vector2();
        this.maxVelocity = maxVelocity;
    }

    @Override
    public void update(float deltaTime) {
        mapCollision(deltaTime);
        move(deltaTime);
        changeDirection();
    }

    private void move(float deltaTime) {
        position.x += velocity.x * deltaTime;
        position.y += velocity.y * deltaTime;
    }

    protected void mapCollision(float deltaTime) {
        Rectangle collisionRect = Utils.rectPool.obtain();
        collisionRect.set(position.x,position.y,width,height);
        // // tile x // //
        int startX, startY, endX, endY;
        if (velocity.x > 0) {
            startX = endX = (int) (position.x + width + velocity.x * deltaTime);
        } else {
            startX = endX = (int) (position.x + velocity.x* deltaTime);
        }
        startY = (int) (position.y);
        endY = (int) (position.y + height);

        getTiles(startX, startY, endX, endY, tiles);
        collisionRect.x += velocity.x * deltaTime;
        for (Rectangle tile : tiles) {
            if (collisionRect.overlaps(tile)) {
                if(this instanceof Player)
                if (Math.abs((int) position.y - position.y) < 0.1f) {
                    position.y = (int) position.y;
                }
                else if (Math.abs((int) position.y + 0.5f - position.y) < 0.1f)
                    position.y = (int) position.y + 0.5f;
                else if(tile.y - collisionRect.y > 0.5f && !map.collide((int)(tile.x*2), (int)(tile.y*2) - 1) &&
                        !map.collide((int)(tile.x*2), (int)(tile.y*2) - 2)) {
                    velocity.y = -maxVelocity;
                }
                else if(collisionRect.y - tile.y < 0.5f && !map.collide((int)(tile.x*2), (int)(tile.y*2) + 1) &&
                        !map.collide((int)(tile.x*2), (int)(tile.y*2) + 2)) {
                    velocity.y = maxVelocity;
                }

                velocity.x = 0;
                break;
            }
        }
        collisionRect.x = position.x;

        // // tile y // //
        if (velocity.y > 0) {
            startY = endY = (int) (position.y + height + velocity.y* deltaTime);
        } else {
            startY = endY = (int) (position.y + velocity.y* deltaTime);
        }
        startX = (int) (position.x);
        endX = (int) (position.x + width);
        getTiles(startX, startY, endX, endY, tiles);

        collisionRect.y += velocity.y * deltaTime;

        for (Rectangle tile : tiles) {
            if (collisionRect.overlaps(tile)) {
                if (Math.abs((int) position.x - position.x) < 0.1f)
                    position.x = (int) position.x;
                else if (Math.abs((int) position.x + 0.5f - position.x) < 0.1f)
                    position.x = (int) position.x + 0.5f;
                else if(tile.x - collisionRect.x > 0.5f && !map.collide((int)(tile.x * 2) - 1,(int) (tile.y * 2)) &&
                        !map.collide((int)(tile.x * 2) - 2, (int)(tile.y * 2))) {
                    velocity.x = -maxVelocity;
                }
                else if(tile.x - collisionRect.x < 0.5f && !map.collide((int)(tile.x * 2) + 1,(int) (tile.y * 2)) &&
                        !map.collide((int)(tile.x * 2) + 2, (int)(tile.y * 2))) {
                    velocity.x = maxVelocity;
                }
                velocity.y = 0;

                break;
            }
        }
        Utils.rectPool.free(collisionRect);
    }

    protected void getTiles(int startX, int startY, int endX, int endY, Array<Rectangle> tiles) {
        Utils.rectPool.freeAll(tiles);
        tiles.clear();
        for (int y = startY; y <= endY; y++) {
            for (int x = startX; x <= endX; x++) {
                map.getData(x, y, tiles);
            }
        }
    }

    private void changeDirection(){
        if (velocity.x != 0 || velocity.y != 0) {
            if (velocity.x < 0)
                dir = Dir.west;
            if (velocity.x > 0)
                dir = Dir.east;
            if (velocity.y < 0)
                dir = Dir.south;
            if (velocity.y > 0)
                dir = Dir.north;
        }
    }

    protected int[] dirToXY(){
        int x = 0,y = 0;
        if(dir == Dir.north)
            y = 1;
        else if(dir == Dir.south)
            y = -1;
        else if(dir == Dir.west)
            x = -1;
        else
            x = 1;
        return new int[]{x,y};
    }

    protected int[] dirToXY(Dir dir){
        int x = 0,y = 0;
        if(dir == Dir.north)
            y = 1;
        else if(dir == Dir.south)
            y = -1;
        else if(dir == Dir.west)
            x = -1;
        else
            x = 1;
        return new int[]{x,y};
    }
}
