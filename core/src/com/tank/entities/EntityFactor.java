package com.tank.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kongo on 10.01.16.
 */
public class EntityFactor {
    public static EntityFactor instance = null;
    private ArrayList<Entity> entities;
    private HashMap<String, Entity> entitiesHashMap;
    private boolean gameover;
    private boolean win;

    private EntityFactor() {
        entities = new ArrayList<Entity>();
        entitiesHashMap = new HashMap<String, Entity>();
    }

    public static EntityFactor getInstance() {
        if(instance == null) {
            instance = new EntityFactor();
        }
        return instance;
    }

    public void init(){
        gameover = false;
        entities.clear();
        entitiesHashMap.clear();
    }

    public void update(float deltaTime){
        for (int i = 0; i < entities.size(); i++) {
            entities.get(i).update(deltaTime);
        }
    }

    public void render(ShapeRenderer shapeRenderer){
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for (int i = 0; i < entities.size(); i++) {
            entities.get(i).render(shapeRenderer);
        }
        shapeRenderer.end();
    }

    public Entity addPlayer(String tag, float x, float y){
        Entity player = new Player(tag, new Vector2(x, y), Color.ROYAL, 1, 1, 3);
        add(tag, player);

        return player;
    }

    public Entity addEnemy(String tag, float x, float y, Player player){
        Entity enemy = new Enemy(tag, new Vector2(x, y), Color.RED, 1, 1, 2, player);
        add(tag, enemy);

        return enemy;
    }

    public Entity addProjectile(String tag, float x, float y, float vx, float vy, Entity shooter, float attackDamage){
        Entity projectile = new Bullet(tag, new Vector2(x,y),Color.MAGENTA, 0.5f, 0.5f, vx, vy, shooter, attackDamage);
        add(tag, projectile);

        return projectile;
    }

    public void remove(Entity entity){
        if(entity instanceof Player) {
            gameover = true;
            win = false;
        }
        else if(entity instanceof Enemy){
            gameover = true;
            win = true;
        }
        entities.remove(entity);
        entitiesHashMap.remove(entity.getTag(), entity);
    }


    private void add(String tag, Entity entity){
        entities.add(entity);
        entitiesHashMap.put(tag, entity);
    }

    public Entity getEntityByTag(String tag) throws TagException {
        Entity entity = entitiesHashMap.get(tag);
        if(entity == null)
            throw new TagException("Can't find entity with tag: " + tag);
        return entitiesHashMap.get(tag);
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }

    public boolean isGameover() {
        return gameover;
    }

    public boolean isWin() {
        return win;
    }
}
