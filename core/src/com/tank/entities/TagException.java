package com.tank.entities;

/**
 * Created by kongo on 12.01.16.
 */
public class TagException extends Exception {
    public TagException(String message) {
        super(message);
    }
}
