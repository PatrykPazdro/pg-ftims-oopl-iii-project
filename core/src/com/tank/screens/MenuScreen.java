package com.tank.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.tank.Main;

/**
 * Created by kongo on 05.12.15.
 */
public class MenuScreen extends MyScreen {
    public MenuScreen(Main main) {
        super(main);
    }

    @Override
    public void show(){
        super.show();

        //buttons
        int buttonWidth = Gdx.graphics.getWidth()/4;
        int buttonHeight = Gdx.graphics.getHeight()/10;

        //game button
        final TextButton gameButton = new TextButton("Start Game", skin, "default");
        gameButton.setPosition(Gdx.graphics.getWidth() /2 - buttonWidth/2, Gdx.graphics.getHeight()/2 + buttonHeight);
        gameButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                main.setScreen(new GameScreen(main));
            }
        });

        //controls button
        final TextButton controlsButton = new TextButton("Controls", skin, "default");
        controlsButton.setPosition(Gdx.graphics.getWidth() /2 - buttonWidth/2, Gdx.graphics.getHeight()/2 - 10);
        controlsButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                main.setScreen(new ControlsScreen(main));
            }
        });

        //exit button
        final TextButton exitButton = new TextButton("Exit", skin, "default");
        exitButton.setPosition(Gdx.graphics.getWidth() /2 - buttonWidth/2, Gdx.graphics.getHeight()/2 - 20 - buttonHeight);
        exitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                Gdx.app.exit();
            }
        });

        stage.addActor(gameButton);
        stage.addActor(controlsButton);
        stage.addActor(exitButton);
    }

    @Override
    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta){
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }


}
