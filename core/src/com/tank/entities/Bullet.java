package com.tank.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.tank.Utils;

/**
 * Created by kongo on 10.01.16.
 */
public class Bullet extends MoveableEntity{
    private EntityFactor entityFactor;
    private Entity shooter;
    private float attackDamage;
    private boolean overlaps = false;

    public Bullet(String tag, Vector2 position, Color color, float width, float height, float vx, float vy, Entity shooter, float attackDamage) {
        super(tag, position, color, width, height, 0);
        velocity.set(vx,vy);
        this.shooter = shooter;
        this.attackDamage = attackDamage;
        entityFactor = EntityFactor.getInstance();
    }

    @Override
    public void update(float deltaTime){
        entitiesCollision();
        super.update(deltaTime);
    }

    private void entitiesCollision(){
        Rectangle collisionRect = Utils.rectPool.obtain();
        collisionRect.set(position.x,position.y,width,height);

        for (int j = 0; j < entityFactor.getEntities().size(); j++) {
            Entity entity = entityFactor.getEntities().get(j);
            if(this == entity)
                continue;

            Rectangle collisionRect2 = Utils.rectPool.obtain();
            collisionRect2.set(entity.position.x,entity.position.y,entity.width,entity.height);

            if(collisionRect.overlaps(collisionRect2) && shooter != entity){
                entityFactor.remove(entity);
                entityFactor.remove(this);
            }
        }
        Utils.rectPool.free(collisionRect);
    }

    @Override
    protected void mapCollision(float deltaTime){
        Rectangle collisionRect = Utils.rectPool.obtain();
        collisionRect.set(position.x,position.y,width,height);

        getTiles((int)collisionRect.x, (int)collisionRect.y, (int)(collisionRect.x + width), (int)(collisionRect.y + height), tiles);
        collisionRect.x += velocity.x * deltaTime;
        for (Rectangle tile : tiles) {
            if (collisionRect.overlaps(tile)) {
                map.tryDestroy((int)(tile.x * 2),(int)(tile.y * 2));
                overlaps = true;
            }
        }
        Utils.rectPool.free(collisionRect);

        if(overlaps)
            entityFactor.remove(this);
    }
}
