package com.tank.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by kongo on 09.01.16.
 */
public class Player extends Tank implements KeyboardInput {
    public Player(String tag, Vector2 position, Color color, float width, float height, float maxVelocity) {
        super(tag, position, color, width, height, maxVelocity);
    }

    @Override
    public void update(float deltaTime){
        input();
        super.update(deltaTime);
    }

    @Override
    public void input() {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            dir = Dir.west;
            velocity.x = -maxVelocity;
            velocity.y = 0;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            dir = Dir.east;
            velocity.x = maxVelocity;
            velocity.y = 0;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            dir = Dir.north;
            velocity.x = 0;
            velocity.y = maxVelocity;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            dir = Dir.south;
            velocity.x = 0;
            velocity.y = -maxVelocity;
        }
        else{
            velocity.x = 0;
            velocity.y = 0;
        }

        if(Gdx.input.isKeyPressed(Input.Keys.Z)){
            shoot();
        }
    }
}
