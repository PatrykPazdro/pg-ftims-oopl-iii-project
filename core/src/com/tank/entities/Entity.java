package com.tank.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by kongo on 09.01.16.
 */
public class Entity {
    protected String tag;
    protected Vector2 position = new Vector2();
    protected Color color;
    protected float width;
    protected float height;

    public Entity(String tag, Vector2 position, Color color, float width, float height) {
        this.tag = tag;
        this.position = position;
        this.color = color;
        this.width = width;
        this.height = height;
    }

    public void update(float deltaTime) {

    }

    public void render(ShapeRenderer shapeRenderer){
        shapeRenderer.setColor(color);
        shapeRenderer.rect(position.x, position.y, width, height);
    }

    public String getTag() {
        return tag;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
