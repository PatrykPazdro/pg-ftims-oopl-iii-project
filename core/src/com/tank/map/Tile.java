package com.tank.map;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by kongo on 09.01.16.
 */
public class Tile {
    private final Color color;
    private final boolean destructible;
    private final boolean passable;

    public Tile(Color color, boolean destructible, boolean passable){
        this.color = color;
        this.destructible = destructible;
        this.passable = passable;
    }

    public Color getColor() {
        return color;
    }

    public boolean isDestructible() {
        return destructible;
    }

    public boolean isPassable() {
        return passable;
    }
}
