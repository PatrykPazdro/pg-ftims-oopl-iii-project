package com.tank.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.tank.Main;

/**
 * Created by kongo on 11.01.16.
 */
public class ControlsScreen extends MyScreen {

    public ControlsScreen(Main main) {
        super(main);
    }

    @Override
    public void show() {
        super.show();

        //buttons
        int buttonWidth = Gdx.graphics.getWidth()/4;
        int buttonHeight = Gdx.graphics.getHeight()/10;
        //back button
        final TextButton backButton = new TextButton("Back", skin, "default");
        backButton.setPosition(Gdx.graphics.getWidth() /2 - buttonWidth/2, Gdx.graphics.getHeight()/2 - 10);
        backButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                main.setScreen(new MenuScreen(main));
            }
        });

        stage.addActor(backButton);
    }

    @Override
    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        batch.begin();
        font.draw(batch, "Moving - arrows", 350, 400);
        font.draw(batch, "Shooting - z", 350, 380);
        batch.end();
    }
}
