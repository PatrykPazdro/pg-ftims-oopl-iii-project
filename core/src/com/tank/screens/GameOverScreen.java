package com.tank.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.tank.Main;

/**
 * Created by kongo on 12.01.16.
 */
public class GameOverScreen extends MyScreen {
    private boolean win;
    private String text;
    private int w;
    public GameOverScreen(Main main, boolean win) {
        super(main);
        this.win = win;
    }

    @Override
    public void show() {
        super.show();
        if(win) {
            font.setColor(Color.GOLD);
            text = "Congratulations, you won!";
            w = 320;
        }
        else {
            font.setColor(Color.RED);
            text = "Game Over";
            w = 360;
        }

        //buttons
        int buttonWidth = Gdx.graphics.getWidth()/4;
        int buttonHeight = Gdx.graphics.getHeight()/10;
        //back button
        final TextButton backButton = new TextButton("Back", skin, "default");
        backButton.setPosition(Gdx.graphics.getWidth() /2 - buttonWidth/2, Gdx.graphics.getHeight()/2 - 10);
        backButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                main.setScreen(new MenuScreen(main));
            }
        });

        stage.addActor(backButton);
    }

    @Override
    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        batch.begin();
        font.draw(batch, text, w, 380);
        batch.end();
    }
}
