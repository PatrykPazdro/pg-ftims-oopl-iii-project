package com.tank;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.tank.screens.GameOverScreen;
import com.tank.screens.MenuScreen;

public class Main extends Game {
	public final static float UNIT_SCALE = 1 / 32f;
	public static int width;
	public static int height;

	@Override
	public void create() {
		width = (int) (Gdx.graphics.getWidth() * UNIT_SCALE);
		height = (int) (Gdx.graphics.getHeight() * UNIT_SCALE);
		setScreen(new MenuScreen(this));
	}
}
