package com.tank.map;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.tank.Utils;

/**
 * Created by kongo on 09.01.16.
 */
public class Map implements TilesData{
    public static Map instance = null;
    private Map(){}
    public static Map getInstance() {
        if(instance == null) {
            instance = new Map();
        }
        return instance;
    }

    private int width;
    private int height;
    private Tile[][] data;

    public void init(int width, int height){
        this.width = width*2;
        this.height = height*2;

        data = new Tile[this.width][this.height];
        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                data[i][j] = null;
            }
        }
        initLevel();
    }

    private void initLevel() {
        for (int i = 0; i < width/2; i++) {
            setData(i,0,border);
            setData(i,height/2-1,border);
        }
        for (int i = 0; i < height/2; i++) {
            setData(0,i,border);
            setData(width/2-1,i,border);
        }

        build(3,1,4,2,metal);
        build(3,height/2-3,4,height/2-2,metal);

        build(width/2 - 5,1,width/2 - 4,2,metal);
        build(width/2 - 5,height/2-3,width/2 - 4,height/2-2,metal);

        build(4,4,width/2 - 5,5, ground);
        build(4,7,width/2 - 5,7,ground);
        build(4,10,width/2 - 5,10,ground);
        build(4,12,width/2 - 5,13,ground);
    }

    public void build(int x,int y, int width,int height, Tile tile) {
        for (int i = x; i <= width; i++) {
            for (int j = y; j <= height; j++) {
                setData(i, j, tile);
            }
        }
    }

    public void tryDestroy(int x,int y){
        if(data[x][y].isDestructible())
            data[x][y] = null;
    }

    public boolean collide(int x, int y){
        if(x  < 0 || x >= width || y < 0 || y >= height) {
            return false;
        }
        else if(data[x][y] != null) {
            if (!data[x][y].isPassable()) {
                return true;
            }
        }
        return false;
    }

    public void setData(int x, int y, Tile tile){
        data[x * 2][y * 2] = tile;
        data[x * 2 + 1][y * 2] = tile;
        data[x * 2][y * 2 + 1] = tile;
        data[x * 2 + 1][y * 2 + 1] = tile;
    }

    public void getData(int x, int y, Array<Rectangle> tiles){
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                if(data[x*2+i][y*2+j] != null){
                    Rectangle rect = Utils.rectPool.obtain();
                    rect.set(x + i * 0.5f, y + j * 0.5f, 0.5f, 0.5f);
                    tiles.add(rect);
                }
            }
        }
    }

    public void render(ShapeRenderer shapeRenderer){
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                if (data[x][y] != null) {
                    shapeRenderer.setColor(data[x][y].getColor());
                    shapeRenderer.rect(x/2 + 0.5f * (x%2), y/2 + 0.5f * (y%2), 0.5f, 0.5f);
                }
            }
        }
        shapeRenderer.end();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}

